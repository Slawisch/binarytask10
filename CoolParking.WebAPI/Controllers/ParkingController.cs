﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        public decimal Balance()
        {
            return _parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public decimal Capacity()
        {
            return _parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public decimal FreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }

 

    }
}
