﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json.Serialization;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;

namespace CoolParking.BL.View
{
    public class ConsoleView : IView
    {
        private readonly HttpClient _client;
        public ConsoleView(HttpClient client)
        {
            _client = client;
        }

        public void PrintBalance()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Balance: " + _client.GetAsync("api/parking/balance").Result.Content.ReadAsStringAsync().Result);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintVehicleById(string vehicleId)
        {
            var response = _client.GetAsync($"api/vehicles/{vehicleId}").Result;

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                PrintErrorMessage("Id is invalid.");
                return;
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                PrintErrorMessage("Vehicle not found.");
                return;
            }

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result));
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintFreePlaces()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Free places: " + _client.GetAsync("api/parking/freePlaces").Result.Content.ReadAsStringAsync().Result);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintLastTransactions()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine(_client.GetAsync("api/transactions/last").Result.Content.ReadAsStringAsync().Result);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintLoggedTransactions()
        {
            var response = _client.GetAsync("api/transactions/all").Result;
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                PrintErrorMessage("Log file not found");
                return;
            }

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintVehicleList()
        {
            var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(_client.GetAsync("api/vehicles").Result.Content.ReadAsStringAsync().Result);
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine(vehicle);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintErrorMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }


        public void PrintMenu()
        {
            Console.WriteLine("1 - Print current parking's balance.");
            Console.WriteLine("2 - Print vehicle by Id.");
            Console.WriteLine("3 - Print amount of free places");
            Console.WriteLine("4 - Print latest transactions.");
            Console.WriteLine("5 - Print logged transactions.");
            Console.WriteLine("6 - Print list of vehicles on parking.");
            Console.WriteLine("7 - Add vehicle on parking.");
            Console.WriteLine("8 - Take away vehicle.");
            Console.WriteLine("9 - Top up vehicle's balance.");
            Console.WriteLine("0 - Exit.");
        }
    }
}
