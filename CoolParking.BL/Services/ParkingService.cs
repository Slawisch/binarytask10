﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private List<TransactionInfo> lastTransactions;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            withdrawTimer.Elapsed += Withdraw;
            logTimer.Elapsed += Log;
            _parking = Parking.GetInstance(withdrawTimer, logTimer, logService);
            lastTransactions = new List<TransactionInfo>();
        }

        public void Dispose()
        {
            _parking.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return (int)_parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return (int)_parking.Capacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count == _parking.Capacity)
                throw new InvalidOperationException("Parking is full.");

            if (_parking.Vehicles.Select(v => v.Id).Contains(vehicle.Id))
                throw new ArgumentException("This vehicle already exists.");

            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle;
            try
            {
                vehicle = _parking.Vehicles.Select(v => v).First(v => v.Id == vehicleId);
            }
            catch
            {
                throw new ArgumentException("This vehicle does not exist.");
            }

            if (vehicle.Balance < 0)
                throw new InvalidOperationException("Can't remove vehicle with negative balance.");

            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Negative balance.");

            try
            {

                Vehicle vehicle = _parking.Vehicles.Select(v => v).First(v => v.Id == vehicleId);
                vehicle.Balance += sum;
            }
            catch
            {
                throw new ArgumentException("Vehicle does not exist.");
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return lastTransactions?.ToArray();
        }

        public string ReadFromLog()
        {
            return _parking.LogService.Read();
        }

        private void Withdraw(object o, ElapsedEventArgs args)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                decimal payment;

                if (_parking.Tariffs[vehicle.VehicleType] <= (float) vehicle.Balance)
                    payment = (decimal)_parking.Tariffs[vehicle.VehicleType];
                else if (vehicle.Balance < 0)
                    payment = (decimal)(_parking.Tariffs[vehicle.VehicleType] * _parking.Fine);
                else
                    payment = vehicle.Balance +
                              (decimal)((_parking.Tariffs[vehicle.VehicleType] - (float)vehicle.Balance) * _parking.Fine);

                vehicle.Balance -= payment;
                _parking.Balance += payment;
                lastTransactions.Add(new TransactionInfo(payment, DateTime.Now, vehicle.Id));

            }
        }

        private void Log(object o, ElapsedEventArgs args)
        {
            TransactionInfo[] transactionsForLog = new TransactionInfo[lastTransactions.Count];
            lastTransactions.CopyTo(transactionsForLog);
            
            foreach (var transactionInfo in transactionsForLog)
            {
                _parking.LogService.Write(transactionInfo.ToString());
            }
            _parking.LogService.Write("");
            lastTransactions = lastTransactions.Except(transactionsForLog).ToList();
        }

        public int GetLastProfit()
        {
            int profit = (int)lastTransactions.Select(t => t.Sum).Sum();
            return profit;
        }
    }
}