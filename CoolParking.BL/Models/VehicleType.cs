﻿namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        Motorcycle,
        Bus,
        Truck,
        PassengerCar
    }
}