﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    static class Settings
    {
        public static int StartBalance { get; } = 0;
        public static int Capacity { get; } = 10;
        public static int WithdrawDelay { get; } = 5;
        public static int LogDelay { get; } = 60;
        public static float Fine { get; } = 2.5f;
        public static Dictionary<VehicleType, float> Tariffs = new Dictionary<VehicleType, float>
        {
            [VehicleType.Bus] = 3.5f,
            [VehicleType.Motorcycle] = 1,
            [VehicleType.PassengerCar] = 2,
            [VehicleType.Truck] = 5
        };
    }
}