﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static Parking _instance;
        public ITimerService WithdrawTimer { get; }
        public ITimerService LogTimer { get; }
        public ILogService LogService { get; }
        public decimal Balance { get; internal set; }
        public decimal Capacity { get; internal set; }
        public IList<Vehicle> Vehicles { get; }
        public float Fine { get; }
        public Dictionary<VehicleType, float> Tariffs { get; }

        protected Parking(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WithdrawTimer = withdrawTimer;
            WithdrawTimer.Interval = Settings.WithdrawDelay;
            WithdrawTimer.Start();

            LogTimer = logTimer;
            LogTimer.Interval = Settings.LogDelay;
            LogTimer.Start();

            LogService = logService;

            Balance = Settings.StartBalance;

            Capacity = Settings.Capacity;

            Vehicles = new List<Vehicle>();

            Fine = Settings.Fine;

            Tariffs = Settings.Tariffs;
        }

        public static Parking GetInstance(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _instance ??= new Parking(withdrawTimer, logTimer, logService);
            return _instance;
        }

        public void Dispose()
        {
            WithdrawTimer.Stop();
            LogTimer.Stop();
            _instance = null;
        }
    }
}