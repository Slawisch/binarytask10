﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.BL.Controllers
{
    public class ConsoleController : IController
    {
        private readonly HttpClient _client;
        private readonly IView _view;
        public ConsoleController(HttpClient client, IView view)
        {
            _client = client;
            _view = view;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            try
            {
                var jsonObject = JsonConvert.SerializeObject(vehicle);
                var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

                var response = _client.PostAsync("api/vehicles", content);

                if(response.Result.StatusCode == HttpStatusCode.BadRequest)
                    _view.PrintErrorMessage("Something went wrong.");
            }
            catch (Exception e)
            {
                _view.PrintErrorMessage(e.Message);
            }
        }

        public void TakeAwayVehicle(string vehicleId)
        {
            try
            {
                var response = _client.DeleteAsync($"api/vehicles/{vehicleId}");

                if(response.Result.StatusCode == HttpStatusCode.BadRequest)
                    _view.PrintErrorMessage("Id is invalid.");

                if (response.Result.StatusCode == HttpStatusCode.NotFound)
                    _view.PrintErrorMessage("Vehicle not found.");
            }
            catch (Exception e)
            {
                _view.PrintErrorMessage(e.Message);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            try
            {
                var jsonObject = JsonConvert.SerializeObject(new{id = vehicleId, Sum = sum});
                var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
                var response = _client.PutAsync("api/transactions/topUpVehicle", content);

                if(response.Result.StatusCode == HttpStatusCode.BadRequest)
                    _view.PrintErrorMessage("Something went wrong.");

                if (response.Result.StatusCode == HttpStatusCode.NotFound)
                    _view.PrintErrorMessage("Vehicle not found.");
            }
            catch (Exception e)
            {
                _view.PrintErrorMessage(e.Message);
            }
        }

        public string GetVehicleId()
        {
            _view.PrintMessage("Input vehicle's Id in format <AB-1234-CD>: ");

            var id = Console.ReadLine();

            return id;
        }

        public VehicleType GetVehicleType()
        {
            _view.PrintMessage($"Input vehicle's type's number: " +
                               $"\n({(int)VehicleType.Bus} - {VehicleType.Bus})" +
                               $"\n({(int)VehicleType.Motorcycle} - {VehicleType.Motorcycle})" +
                               $"\n({(int)VehicleType.PassengerCar} - {VehicleType.PassengerCar})" +
                               $"\n({(int)VehicleType.Truck} - {VehicleType.Truck})");

            var type = (VehicleType)int.Parse(Console.ReadLine() ?? string.Empty);

            return type;
        }

        public decimal GetVehicleBalance()
        {
            _view.PrintMessage("Input vehicle's balance: ");

            var balance = decimal.Parse(Console.ReadLine() ?? string.Empty);

            return balance;
        }


        public void AddVehicle()
        {
            try
            {
                var id = GetVehicleId();
                var type = GetVehicleType();
                var balance = GetVehicleBalance();

                AddVehicle(new Vehicle(id, type, balance));
            }
            catch (Exception e)
            {
                _view.PrintErrorMessage(e.Message);
            }
        }

        public void TakeAwayVehicle()
        {
            try
            {
                var id = GetVehicleId();

                TakeAwayVehicle(id);
            }
            catch (Exception e)
            {
                _view.PrintErrorMessage(e.Message);
            }
        }

        public void TopUpVehicle()
        {
            try
            {
                var id = GetVehicleId();
                var balance = GetVehicleBalance();

                TopUpVehicle(id, balance);
            }
            catch (Exception e)
            {
                _view.PrintErrorMessage(e.Message);
            }
        }

        public void StartWork()
        {
            bool isWorking = true;
            while (isWorking)
            {
                _view.PrintMenu();
                isWorking = DoAction(Console.ReadLine());
            }
        }

        private bool DoAction(string choice)
        {
            switch (choice)
            {
                case "0":
                    return false;
                case "1":
                    _view.PrintBalance();
                    break;
                case "2":
                    _view.PrintVehicleById(GetVehicleId());
                    break;
                case "3":
                    _view.PrintFreePlaces();
                    break;
                case "4":
                    _view.PrintLastTransactions();
                    break;
                case "5":
                    _view.PrintLoggedTransactions();
                    break;
                case "6":
                    _view.PrintVehicleList();
                    break;
                case "7":
                    AddVehicle();
                    break;
                case "8":
                    TakeAwayVehicle();
                    break;
                case "9":
                    TopUpVehicle();
                    break;
                default:
                    return true;
            }
            return true;
        }
    }
}
