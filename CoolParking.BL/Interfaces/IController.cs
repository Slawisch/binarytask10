﻿using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface IController
    {
        void AddVehicle(Vehicle vehicle);
        void TakeAwayVehicle(string vehicleId);
        void TopUpVehicle(string vehicleId, decimal sum);
        void StartWork();
    }
}
