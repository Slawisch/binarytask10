﻿using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;
using CoolParking.BL.Controllers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.View;
using Newtonsoft.Json;

namespace CoolParkingApp
{
    class Program
    {
        private const string APP_PATH = "https://localhost:44365/";
        private static IController controller;
        private static IView view;
        public static void Main()
        {
            
            var client = new HttpClient();
            client.BaseAddress = new Uri(APP_PATH);
            view = new ConsoleView(client);
            controller = new ConsoleController(client, view);

            controller.StartWork();
        }


    }
}